package com.epam.controller;

import com.epam.model.Fibonacci;
import com.epam.model.CriticalClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.*;

public class Executor {
    private static Logger logger = LogManager.getLogger(Executor.class);
    private static Random random;
    private static final int MAX_LENGTH_OF_SEQUENCE;
    private static final int MIN_LENGTH_OF_SEQUENCE;
    private static final int DELAY;
    private static final int SLEEP_MAX;
    private PipedOutputStream pipedOutput;
    private PipedInputStream pipedInput;

    static {
        ResourceBundle bundle = ResourceBundle.getBundle("config");
        MAX_LENGTH_OF_SEQUENCE = Integer.valueOf(bundle.getString("MAX_COUNT"));
        MIN_LENGTH_OF_SEQUENCE = Integer.valueOf(bundle.getString("MIN_COUNT"));
        DELAY = Integer.valueOf(bundle.getString("DELAY"));
        SLEEP_MAX = Integer.valueOf(bundle.getString("SLEEP_MAX"));
        random = new Random();
    }

    public Executor() {
        try {
            pipedOutput = new PipedOutputStream();
            pipedInput = new PipedInputStream();
            pipedInput.connect(pipedOutput);
        } catch (IOException e) {
            logger.info("IO troubles...\n");
        }
    }

    private Runnable createRunnable() {
        return () -> {
            int count = random.nextInt(MAX_LENGTH_OF_SEQUENCE - MIN_LENGTH_OF_SEQUENCE)
                    + MIN_LENGTH_OF_SEQUENCE;
            Fibonacci sequence = new Fibonacci(count);
            logger.info(Thread.currentThread().getName() +
                    ". Created new fibonacci sequence: " + sequence.getSequence() + "\n");
        };
    }

    private Callable<Integer> createCallable() {
        return () -> {
            int count = random.nextInt(MAX_LENGTH_OF_SEQUENCE - MIN_LENGTH_OF_SEQUENCE)
                    + MIN_LENGTH_OF_SEQUENCE;
            Fibonacci sequence = new Fibonacci(count);
            return sequence.getSequence().stream().mapToInt(Integer::intValue).sum();
        };
    }

    public void createFibonacciTasks() throws InterruptedException {
        logger.info("\t\tCreating " + MAX_LENGTH_OF_SEQUENCE + " threads...\n");
        for (int i = 0; i < MAX_LENGTH_OF_SEQUENCE; i++) {
            logger.info("Creating new thread: " + LocalTime.now() + "\n");
            Thread thread = new Thread(createRunnable());
            thread.start();
            thread.join();
        }
    }

    public void createCallableTasks() {
        logger.info("\t\tCreating " + MAX_LENGTH_OF_SEQUENCE + " threads using Callable...\n");
        ExecutorService service = Executors.newSingleThreadExecutor();
        for (int i = 0; i < MAX_LENGTH_OF_SEQUENCE; i++) {
            logger.info("Callable." + i + ".Creating new thread: " + LocalTime.now() + "\n");
            try {
                logger.info("Sum of the fibonacci numbers: " + service.submit(createCallable()).get() + "\n");
            } catch (InterruptedException | ExecutionException e) {
                logger.info("Troubles with execution\n");
            }
        }
        service.shutdown();
    }

    public void createFibonacciWithExecutors() {
        logger.info("\t\tCreating " + MAX_LENGTH_OF_SEQUENCE + " threads with Executors...\n");
        ExecutorService service = Executors.newSingleThreadExecutor();
        for (int i = 0; i < MAX_LENGTH_OF_SEQUENCE; i++) {
            service.submit(createRunnable());
            logger.info(i + ".Creating new thread: " + LocalTime.now() + "\n");
        }
        service.shutdown();
    }

    public void createScheduleExecutors() throws InterruptedException {
        logger.info("\t\tCreating Schedule executors with delay " + DELAY + "...\n");
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(MAX_LENGTH_OF_SEQUENCE);
        for (int i = 0; i < MAX_LENGTH_OF_SEQUENCE; i++) {
            ScheduledFuture<?> future = executor.schedule(createRunnable(), DELAY, TimeUnit.SECONDS);
            TimeUnit.SECONDS.sleep(DELAY);
            logger.info("SCHEDULE." + i + ".Creating new Schedule thread: " + LocalTime.now() + "\n");
        }
        executor.shutdown();
    }

    public void sleepThreadsWithSchedulePool(int quantity) throws InterruptedException {
        logger.info("Given " + quantity + " threads...\n");
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(quantity);
        List<Callable<Integer>> callables = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            callables.add(createSleepingThread());
        }
        executor.invokeAll(callables).stream()
                .map(future -> {
                    try {
                        return future.get();
                    } catch (Exception e) {
                        throw new IllegalStateException(e);
                    }
                }).forEach(s -> logger.info("Sleeping time: " + s + " milliSeconds\n"));
        executor.shutdown();
    }

    private Callable<Integer> createSleepingThread() {
        return () -> {
            int sleepTime = random.nextInt(SLEEP_MAX);
            try {
                TimeUnit.MILLISECONDS.sleep(sleepTime);
            } catch (InterruptedException e) {
                logger.info("Interrupted!\n");
            }
            return sleepTime;
        };
    }

    public void testCriticalSection() throws InterruptedException {
        CriticalClass criticalClass = new CriticalClass();
        criticalClass.startDealWithDifferentSyncObj();
        Thread.sleep(1000);
        criticalClass.startDealWithSameSyncObj();
        Thread.sleep(1000);
    }

    private Thread writeToPipeOutput() throws IOException {
        return new Thread(() -> {
            logger.info("Using pipeOutputStream to write message to another thread...\n");
            for (int i = MAX_LENGTH_OF_SEQUENCE * 2; i < MAX_LENGTH_OF_SEQUENCE * 3; i++) {
                logger.info("...Sending data: " + (char) i + "\n");
                try {
                    pipedOutput.write(i);
                } catch (IOException e) {
                    logger.info("IO troubles with writing...\n");
                }
            }
            logger.info("All data is written!\n");
        });
    }

    private Thread readFromPipeInput() throws IOException {
        return new Thread(() -> {
            logger.info("Use pipeInputStream to read data from another thread...\n");
            for (int i = MAX_LENGTH_OF_SEQUENCE * 2; i < MAX_LENGTH_OF_SEQUENCE * 3; i++) {
                try {
                    logger.info("...Reading data: " + (char) pipedInput.read() + "\n");
                } catch (IOException e) {
                    logger.info("OI troubles with reading...\n");
                }
            }
            logger.info("All data is read!\n");
        });
    }

    public void usePipeCommunication() throws IOException, InterruptedException {
        Thread pipeWriter = writeToPipeOutput();
        Thread pipeReader = readFromPipeInput();
        pipeWriter.start();
        pipeReader.start();
        pipeWriter.join();
        pipeReader.join();
    }
}
