package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalTime;

public class PingPong {
    private static Logger logger = LogManager.getLogger(PingPong.class);
    private String ball;
    private final Object sync;
    private final long forCount;
    private Thread first;
    private Thread second;

    public PingPong() {
        sync = new Object();
        ball = "Ping";
        forCount = 100;
        createFirstThread();
        createSecondThread();
    }

    public void play() {
        logger.info("Time: " + LocalTime.now() + "\n");
        first.start();
        second.start();
        try {
            first.join();
            second.join();
        } catch (InterruptedException e) {
            logger.info("Ping Pong was Interrupted!\n");
        }
        logger.info("Time: " + LocalTime.now() + "\n");
    }

    private void createFirstThread() {
        first = new Thread(() -> {
            synchronized (sync) {
                for (int i = 0; i < forCount; i++) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        logger.info("Ping Pong was Interrupted!\n");
                    }
                    ball = "Ping" + i;
                    logger.info(ball + " ");
                    sync.notify();
                }
            }
            logger.info("Game Over for " + Thread.currentThread().getName() + "\n");
        });
    }

    private void createSecondThread() {
        second = new Thread(() -> {
            synchronized (sync) {
                for (int i = 0; i < forCount; i++) {
                    sync.notify();
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        logger.info("Ping Pong was Interrupted!\n");
                    }
                    ball = "Pong" + i;
                    logger.info(ball + "\n");
                }
            }
            logger.info("Game Over for " + Thread.currentThread().getName() + "\n");
        });
    }

}
