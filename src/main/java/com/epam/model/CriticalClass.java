package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalTime;
import java.util.ResourceBundle;

public class CriticalClass {
    private static Logger logger = LogManager.getLogger(CriticalClass.class);
    private static final Object sync = new Object();
    private static final Object synchron = new Object();
    private static final Object synchronize = new Object();
    private static final int repeatedTime;
    private static int number;

    static {
        ResourceBundle bundle = ResourceBundle.getBundle("config");
        repeatedTime = Integer.valueOf(bundle.getString("SLEEP_MAX"));
        number = 0;
    }

    private void dealWithCriticalSection() throws InterruptedException {
        Thread thread = new Thread(() -> {
            synchronized (sync) {
                logger.info(Thread.currentThread().getName()
                        + ": Starting to increment the number in first method...\n");
                for (int i = 0; i < repeatedTime; i++) {
                    number++;
                }
                logger.info("Finished incrementing in first method in "
                        + LocalTime.now() + " with result = " + number + "\n");
            }
        });
        thread.start();
    }

    private void secondDealWithCriticalSection(Object sync) throws InterruptedException {
        Thread thread = new Thread(() -> {
            synchronized (sync) {
                logger.info(Thread.currentThread().getName()
                        + ": Starting to increment the number in second method...\n");
                for (int i = 0; i < repeatedTime; i++) {
                    number++;
                }
                logger.info("Finished incrementing in second method in "
                        + LocalTime.now() + " with result = " + number + "\n");
            }
        });
        thread.start();
    }

    private void thirdDealWithCriticalSection(Object sync) throws InterruptedException {
        Thread thread = new Thread(() -> {
            synchronized (sync) {
                logger.info(Thread.currentThread().getName()
                        + ": Starting to increment the number in third method...\n");
                for (int i = 0; i < repeatedTime; i++) {
                    number++;
                }
                logger.info("Finished incrementing in third method in "
                        + LocalTime.now() + " with result = " + number + "\n");
            }
        });
        thread.start();
    }

    public void startDealWithSameSyncObj() throws InterruptedException {
        logger.info("Dealing with same monitors...\n");
        dealWithCriticalSection();
        secondDealWithCriticalSection(sync);
        thirdDealWithCriticalSection(sync);
        logger.info("Finished work with the thread!");
    }

    public void startDealWithDifferentSyncObj() throws InterruptedException {
        logger.info("Dealing with different monitors...\n");
        dealWithCriticalSection();
        secondDealWithCriticalSection(synchron);
        thirdDealWithCriticalSection(synchronize);
    }

}
