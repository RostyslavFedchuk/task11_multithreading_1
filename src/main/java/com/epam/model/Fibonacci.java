package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Fibonacci {
    private static Logger logger = LogManager.getLogger(Fibonacci.class);
    private List<Integer> sequence;

    public Fibonacci(int countFibNumbers) {
        sequence = new ArrayList<>();
        produceSequence(countFibNumbers);
    }

    private int buildFibonacci(int N) {
        if (N < 0) {
            return -1;
        }

        if (N == 0 || N == 1) {
            return 1;
        }
        return buildFibonacci(N - 1) + buildFibonacci(N - 2);
    }

    private void produceSequence(int countNumbers) {
        if (countNumbers < 1) {
            logger.info("Cannot deal with negative numbers!\n");
            return;
        }
        for (int i = 0; i < countNumbers; i++) {
            sequence.add(buildFibonacci(i));
        }
    }

    public synchronized List<Integer> getSequence() {
        return sequence;
    }
}
