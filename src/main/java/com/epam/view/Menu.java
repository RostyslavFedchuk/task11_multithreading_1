package com.epam.view;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class Menu {
    private static final int COUNT_OPTIONS;
    private static Map<String, String> menu;

    private static ResourceBundle bundle;

    static {
        Locale locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        COUNT_OPTIONS = Integer.valueOf(bundle.getString("COUNT_OPTIONS"));
        setMenu();
    }

    private static void setMenu(){
        menu = new LinkedHashMap<>();
        for (int i = 1; i <= COUNT_OPTIONS; i++) {
            menu.put("" + i, bundle.getString("" + i));
        }
        menu.put("Q", bundle.getString("Q"));
    }

    static Map<String, String> getMenu(){
        return menu;
    }
}
