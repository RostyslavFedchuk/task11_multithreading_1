package com.epam.view;

import com.epam.controller.Executor;
import com.epam.model.PingPong;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static Logger logger = LogManager.getLogger(MyView.class);
    private static final Scanner SCANNER = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;

    private Executor executor;

    public MyView() {
        executor = new Executor();
        menu = Menu.getMenu();
        setMenuMethods();
    }

    private void setMenuMethods() {
        menuMethods = new LinkedHashMap<>();
        menuMethods.put("1", this::playPingPong);
        menuMethods.put("2", this::createFibonacciTasks);
        menuMethods.put("3", this::createFibonacciWithExecutors);
        menuMethods.put("4", this::createCallableTasks);
        menuMethods.put("5", this::sleepThreadsWithSchedulePool);
        menuMethods.put("6", this::testCriticalSection);
        menuMethods.put("7", this::usePipeCommunication);
        menuMethods.put("Q", this::quit);
    }

    private void printMenu() {
        logger.info("__________________"
                + "THREAD MENU__________________\n");
        for (Map.Entry entry : menu.entrySet()) {
            logger.info(entry.getKey() + " - " + entry.getValue() + "\n");
        }
        logger.info("__________________________________________________\n");
    }

    private void playPingPong() {
        new PingPong().play();
    }

    private void createFibonacciTasks() throws InterruptedException {
        executor.createFibonacciTasks();
    }

    private void createFibonacciWithExecutors() throws InterruptedException {
        executor.createFibonacciWithExecutors();
        executor.createScheduleExecutors();
    }

    private void createCallableTasks() {
        executor.createCallableTasks();
    }

    private void sleepThreadsWithSchedulePool() throws InterruptedException {
        logger.info("Enter the count of the threads: ");
        int quantity = SCANNER.nextInt();
        if (quantity < 1) {
            logger.info("Cannot be less than 1!\n");
        } else {
            executor.sleepThreadsWithSchedulePool(quantity);
        }
    }

    private void testCriticalSection() throws InterruptedException {
        executor.testCriticalSection();
    }

    private void usePipeCommunication() throws IOException, InterruptedException {
        executor.usePipeCommunication();
    }

    private void quit() {
        logger.info("Bye!");
    }

    public void show() {
        String option = "";
        do {
            printMenu();
            logger.info("Choose one option: ");
            option = SCANNER.next().toUpperCase();
            if (menuMethods.containsKey(option)) {
                try {
                    menuMethods.get(option).print();
                } catch (InterruptedException e) {
                    logger.info("Was interrupted!\n");
                } catch (IOException e) {
                    logger.info("Troubles with IO\n");
                }
            } else {
                logger.info("Wrong input! Try again.");
            }
        } while (!option.equals("Q"));
    }
}
